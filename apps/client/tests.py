from django.test import TestCase
from django.contrib.auth.models import User


class ClientClass(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        User.objects.create(username='Big', password='Bafadf12ob')

    def test_first_name_label(self):
        author = ClientClass.objects.get(id=1)
        field_label = author._meta.get_field('username').verbose_name
        self.assertEquals(field_label, 'username')
