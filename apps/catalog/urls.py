
from django.urls import path
from apps.catalog import views

app_name = 'catalogs'

urlpatterns = [
    path('create-data/', views.process_data, name='process_data'),
]