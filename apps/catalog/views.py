from django.http import HttpResponse
from django.contrib.auth.models import User, Group, Permission
from apps.client.models import Client

# Create your views here.
import csv, pyexcel


def process_data(request):
    group = Group.objects.create(name='POWERPUFF GIRLS')

    path_client = 'static/catalogs/data_clientes.csv'  # clientes
    with open(path_client, mode='r') as csv_client:
        csv_reader = csv.reader(csv_client, delimiter=',', quotechar=" ")
        count = 1
        for row in csv_reader:
            username = str(row[0])
            password = str(row[1])
            email = str(row[2])
            """ Create user """
            user = User.objects.create_user(
                username=username,
                email=email,
                password=password
            )
            Client.objects.create(user=user)
            if count == 1:
                """ Establece un coordinador """
                client = Client.objects.get(user=user)
                client.coordinator = True  # set coordinator
                client.save()
                permission = Permission.objects.get(codename='coordinator')
                user.user_permissions.add(permission)
            group.user_set.add(user)  # add client to new group
            count += 1

    return HttpResponse('Process')
